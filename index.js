require('dotenv').config()
const express = require("express");
const mongoose = require("mongoose");
const userRouter = require("./Routers/userRouter")
const roleRouter = require("./Routers/rolesRouter");
const tagRouter = require("./Routers/tagRouter")
const categoriesRouter = require("./Routers/catgRouter")
const productRouter = require("./Routers/productRouter")
const cartRouter = require("./Routers/cartRouter")
const orderRouter = require("./Routers/orderRouter")
const loginRouter = require("./Routers/loginRouter")
const searchRouter = require("./Routers/searchRouter")

const app = express();
const db = mongoose.connection;
const port = process.env.PORT;
// const port = 8080;

app.use(express.json());
app.use(
    express.urlencoded({
        extended: true,
    })
);

app.get("/", (req, res) => {
    res.send("<h1>Welcome</h1>")
});

//Using Routers
app.use(userRouter)
app.use(roleRouter)
app.use(categoriesRouter)
app.use(tagRouter)
app.use(productRouter)
app.use(cartRouter)
app.use(orderRouter)
app.use(loginRouter)
app.use(searchRouter)

//Creating Server and connecting to Database
app.listen(port, async () => {
    try {
        await mongoose.connect(process.env.DB_URL);
        db.on("error", () => console.error.bind(console, "Connection Error:"))
        db.once("open", function () {
            console.log('Connected sucssessfully')
        });

    } catch (error) {
        console.log("Something went wrong");
    }
    console.log(`Server runing on port ${port}`);

});