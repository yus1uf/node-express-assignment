
const productModel = require("../model/productModel")

//Searching Products based on Tags and Categories
const searchProduct = async (req, res) => {
    try {
        var regex = new RegExp(req.params.name, "i")
        const query = { $or: [{ Category_name: regex }, { Tags: regex }] }
        const result = await productModel.find(query)
        if (!result) {
            res.status(401).send("Product Not Found")
        }
        res.status(200).send(result)
    } catch (error) {
        res.status(401).send(error)
    }
}

module.exports = searchProduct