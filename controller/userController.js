const userModel = require("../model/userModel");
const bcrypt = require("bcrypt");

//storing data into database
const storeUser = async (req, res) => {
    try {
        //Password encryption using bcrypt
        const encryptedPass = bcrypt.hashSync(req.body.password, 10);

        const user = await new userModel({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            role: req.body.role,
            profile_image: req.body.profile_image,
            password: encryptedPass
        });
        user.save();

        res.status(201).send(user)
    } catch (error) {
        console.log(error)
    }

}

//Reading data from database
const getUser = async (req, res) => {
    try {
        const users = await userModel.find({});

        res.status(200).send(users)
    } catch (error) {
        console.log(error)
    }
}

//Reading data from database based on query params
const getDetails = async (req, res) => {
    try {
        const id = req.params.id;
        const userDetails = await userModel.find({ _id: id });
        res.status(200).send(userDetails);
    } catch (error) {
        console.log(error)
    }
}

//Updating data into database
const updateDetails = async (req, res) => {
    try {
        const filter = { _id: req.params.id };
        const query = { $set: { first_name: req.body.first_name, last_name: req.body.last_name } };

        const updateUser = await userModel.updateOne(filter, query);

        res.status(201).send(updateUser)
    } catch (error) {
        console.log(error)
    }
}

//Deleting data from database
const deleteRecord = async (req, res) => {
    try {
        const filter = { _id: req.params.id };
        const deleted = await userModel.deleteOne(filter);
        res.status(200).send(deleted);
    } catch (error) {
        console.log(error)
    }
}



module.exports = {
    storeUser,
    getUser,
    getDetails,
    updateDetails,
    deleteRecord
}