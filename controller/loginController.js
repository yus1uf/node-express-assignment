const userModel = require("../model/userModel")
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken")

const login= async (req,res)=>{
   try {
    const user=await userModel.findOne({email:req.body.email})
    if(!user){
        res.send("User not found");
    }
    else{
       const match = await bcrypt.compare(req.body.password,user.password)
       if(!match){
           res.status(401).send("Password did not matched..!!")
       }
       else{
           const payload = user._id;
           const secret = process.env.SECRET
           jwt.sign({payload},secret,{expiresIn:process.env.EXPIRE_IN},(err,token)=>{
               if(err){
                   res.status(401).send(err)
               }
               else{
                   res.send(token)
               }
           })

       }
    }
   } catch (error) {}
}

module.exports = {login}