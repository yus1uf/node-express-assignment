const roleModel = require("../model/rolesModel");

//Inserting data from database
const storeRole = async (req, res) => {
    try {
        const role = await new roleModel({ ...req.body });
        role.save();
        res.status(201).send(role);
    } catch (error) {
        console.log(error)
    }
}

//Reading data from database
const getRole = async (req, res) => {
    try {
        const roles = await roleModel.find({});
        res.status(200).send(roles);
    } catch (error) {
        console.log(error)
    }
}

//Reading data from database based on query params
const getRoleDetails = async (req, res) => {
    try {
        const id = req.params.id;
        const roleDetail = await roleModel.find({ _id: id })
        res.status(200).send(roleDetail);
    } catch (error) {
        console.log(error)
    }
}

//Updating data from database
const updateRole = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const query = { $set: { Name: req.body.Name } }
        const roleUpdated = await roleModel.updateOne(filter, query);
        res.status(201).send(roleUpdated);
    } catch (error) {
        console.log(error)
    }
}

//Deleting data from database
const deleteRole = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        roleDeleted = await roleModel.deleteOne(filter);
        res.status(200).send(roleDeleted)
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    storeRole,
    getRole,
    getRoleDetails,
    updateRole,
    deleteRole
}