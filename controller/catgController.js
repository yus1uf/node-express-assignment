const categoriesModel = require("../model/catgModel");

//Inserting data into database
const storeCategories = async (req, res) => {
    try {
        const categories = await new categoriesModel({ ...req.body });
        categories.save();
        res.status(201).send(categories);
    } catch (error) {
        console.log(error)
    }
}

//Reading data from database
const getCategories = async (req, res) => {
    try {
        const getCategories = await categoriesModel.find({});
        res.status(200).send(getCategories);
    } catch (error) {
        console.log(error)
    }
}

//Reading data from database based on query params
const getCatDetails = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const getDetails = await categoriesModel.find(filter);
        res.status(200).send(getDetails);
    } catch (error) {
        console.log(error)
    }
}

//updating data into database
const updateCategory = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const query = { $set: { Name: req.body.Name, slug: req.body.slug } }
        const updateCategoryDetails = await categoriesModel.updateOne(filter, query);
        res.status(201).send(updateCategoryDetails);
    } catch (error) {
        console.log(error)
    }
}

//Deleting data from database
const deleteCategory = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const deleteCategoryDetails = await categoriesModel.deleteOne(filter);
        res.status(201).send(deleteCategoryDetails);
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    storeCategories,
    getCategories,
    getCatDetails,
    updateCategory,
    deleteCategory
}