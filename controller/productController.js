const productModel = require("../model/productModel");

//Inserting data from database
const storeProduct = async (req, res) => {
    try {
        const productStore = await new productModel({ ...req.body });
        productStore.save();
        res.status(201).send(productStore);
    } catch (error) {
        console.log(error)
    }
}

//Reading data from database
const getProducts = async (req, res) => {
    try {
        const getProduct = await productModel.find({});
        res.status(200).send(getProduct)
    } catch (error) {
        console.log(error)
    }
}

//Reading data from database based on query params
const getDetails = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const productDetails = await productModel.find(filter)
        res.status(200).send(productDetails)
    } catch (error) {
        console.log(error)
    }
}

//Updating data from database
const updateProduct = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const query = { $set: { Base_Price: req.body.Base_Price, Sell_Price: req.body.Sell_Price } }
        const productUpdate = await productModel.updateOne(filter, query)
        res.status(201).send(productUpdate)
    } catch (error) {
        console.log(error)
    }
}

//Deleting data from database
const deleteProduct = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const productDelete = await productModel.deleteOne(filter);
        res.status(201).send(productDelete)
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    storeProduct,
    getProducts,
    getDetails,
    updateProduct,
    deleteProduct
}