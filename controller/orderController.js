const orderModel = require("../model/orderModel");

//Inserting data from database
const storeOrder = async (req, res) => {
    try {
        const orderStore = await new orderModel({ ...req.body });
        orderStore.save();
        res.status(201).send(orderStore);
    } catch (error) { }
}

//Reading data from database
const getOrder = async (req, res) => {
    try {
        const getOrder = await orderModel.find({});
        res.status(200).send(getOrder)
    } catch (error) { }
}

//Reading data from database based on query params
const getDetails = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const details = await orderModel.find(filter)
        res.status(200).send(details)
    } catch (error) { }
}

//Updating data into database
const updateOrder = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const query = {
            $set: {
                Transaction_Status: req.body.Transaction_Status,
                Payment_mode: req.body.Payment_mode,
                Payment_Status: req.body.Payment_Status,
                Order_Status: req.body.Order_Status
            }
        }
        const orderUpdate = await orderModel.updateOne(filter, query)
        res.status(201).send(orderUpdate)
    } catch (error) { }
}

//Deleting data from database
const deleteOrder = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const orderDelete = await orderModel.deleteOne(filter);
        res.status(201).send(orderDelete)
    } catch (error) { }
}

module.exports = {
    storeOrder,
    getOrder,
    getDetails,
    updateOrder,
    deleteOrder
}