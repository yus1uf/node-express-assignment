const cartModel = require("../model/cartModel");

//storing data into database
const storeCart = async (req, res) => {
    try {
        const cartStore = await new cartModel({...req.body });
        cartStore.save();
        res.status(201).send(cartStore);
    } catch (error) { }
}

//Reading data from database
const getCart = async (req, res) => {
    try {
        const getCart = await cartModel.find({});
        res.status(200).send(getCart)
    } catch (error) { }
}

//Reading data from database bassed on Query params
const getDetails = async (req,res)=>{
    try {
        const filter = {_id:req.params.id}
        const details = await cartModel.find(filter)
        res.status(200).send(details)
    } catch (error) { }
}

//Updating data into database
const updateCart = async (req,res)=>{
    try {
        const filter = {_id:req.params.id}
        const query = {$set:{Base_Price:req.body.Base_Price,
            Sell_Price:req.body.Sell_Price,
            Total_Price:req.body.Total_Price}}
        const cartUpdate = await cartModel.updateOne(filter,query)
        res.status(201).send(cartUpdate)
    } catch (error) { }
}

//Deleting Data from database
const deleteCart = async (req,res)=>{
    try {
        const filter = {_id:req.params.id}
        const cartDelete = await cartModel.deleteOne(filter);
        res.status(201).send(cartDelete)
    } catch (error) { }
}

module.exports = {
    storeCart,
    getCart,
    getDetails,
    updateCart,
    deleteCart
}