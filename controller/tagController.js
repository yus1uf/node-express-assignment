const tagModel = require("../model/tagModel");

//Inserting data from database
const storeTag = async (req, res) => {
    try {
        const storeTag = await new tagModel({ ...req.body });
        storeTag.save();
        res.status(201).send(storeTag);
    } catch (error) { }
}

//Reading data from database
const getTags = async (req, res) => {
    try {
        const getTag = await tagModel.find({});
        res.status(200).send(getTag)
    } catch (error) { }
}

//Reading data from database based on query params
const getDetails = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const tagDetails = await tagModel.find(filter)
        res.status(200).send(tagDetails)
    } catch (error) { }
}

//Updating data from database
const updateTag = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const query = { $set: { Name: req.body.Name } }
        const tagsUpdate = await tagModel.updateOne(filter, query)
        res.status(201).send(tagsUpdate)
    } catch (error) { }
}

//Deleting data from database
const deleteTag = async (req, res) => {
    try {
        const filter = { _id: req.params.id }
        const tagDelete = await tagModel.deleteOne(filter);
        res.status(201).send(tagDelete)
    } catch (error) {

    }
}

module.exports = {
    storeTag,
    getTags,
    getDetails,
    updateTag,
    deleteTag
}