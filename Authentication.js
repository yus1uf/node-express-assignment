const jwt = require("jsonwebtoken");
const isAuthenticated = (req, res, next) => {
    const bearerHeadr = req.headers['authorization'];
    const bearer = bearerHeadr.split(" ")[1];
    req.token = bearer;

    //verifying the Token generated from login
    jwt.verify(req.token, process.env.SECRET, (err, auth) => {
        if (err) {
            res.status(401).send(err)
        }
        else {
            next();
        }
    })
}

module.exports = isAuthenticated;