const express = require("express")
const isAuthenticated = require("../Authentication")

const Router = express.Router();

const {
    storeTag,
    getTags,
    getDetails,
    updateTag,
    deleteTag
} = require("../controller/tagController")

Router.post("/tags", isAuthenticated,storeTag)
Router.get("/tags",isAuthenticated, getTags)
Router.get("/tags/:id",isAuthenticated, getDetails)
Router.put("/tags/:id",isAuthenticated,updateTag)
Router.delete("/tags/:id",isAuthenticated,deleteTag)


module.exports = Router;