const express = require("express");
const isAuthenticated = require("../Authentication")

const Router = express.Router();

const {
    storeCategories,
    getCategories,
    getCatDetails,
    updateCategory,
    deleteCategory
} = require("../controller/catgController");

Router.post("/categories",isAuthenticated,storeCategories);
Router.get("/categories",isAuthenticated,getCategories);
Router.get("/categories/:id",isAuthenticated,getCatDetails);
Router.put("/categories/:id",isAuthenticated,updateCategory);
Router.delete("/categories/:id",isAuthenticated,deleteCategory)

module.exports=Router;