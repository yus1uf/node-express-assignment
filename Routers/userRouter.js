const express = require("express");
const isAuthenticated = require("../Authentication")
const Router = express.Router();

const { storeUser,
    getUser,
    getDetails,
    updateDetails,
    deleteRecord
} = require("../controller/userController")

Router.get("/users", isAuthenticated,getUser);

Router.post("/users", storeUser);

Router.get("/users/:id",isAuthenticated, getDetails);

Router.put("/users/:id",isAuthenticated, updateDetails)

Router.delete("/users/:id", isAuthenticated,deleteRecord)

module.exports = Router;