const express = require("express")
const isAuthenticated = require("../Authentication")

const Router = express.Router();

const searchProduct=require("../controller/searchController")

Router.get("/search/:name",isAuthenticated,searchProduct)

module.exports = Router;