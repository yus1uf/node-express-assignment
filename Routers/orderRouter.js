const express = require("express")
const isAuthenticated = require("../Authentication")

const Router = express.Router();

const {
    storeOrder,
    getOrder,
    getDetails,
    updateOrder,
    deleteOrder
} = require("../controller/orderController")

Router.post("/orders", isAuthenticated,storeOrder)
Router.get("/orders",isAuthenticated, getOrder)
Router.get("/orders/:id",isAuthenticated, getDetails)
Router.put("/orders/:id",isAuthenticated, updateOrder)
Router.delete("/orders/:id",isAuthenticated,deleteOrder)


module.exports = Router;