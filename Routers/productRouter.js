const express = require("express")
const isAuthenticated = require("../Authentication")

const Router = express.Router();

const {
    storeProduct,
    getProducts,
    getDetails,
    updateProduct,
    deleteProduct
} = require("../controller/productController")

Router.post("/products", isAuthenticated,storeProduct)
Router.get("/products",isAuthenticated, getProducts)
Router.get("/products/:id", isAuthenticated,getDetails)
Router.put("/products/:id",isAuthenticated,updateProduct)
Router.delete("/products/:id",isAuthenticated,deleteProduct)


module.exports = Router;