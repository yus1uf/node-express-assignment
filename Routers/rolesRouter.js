const express = require("express")
const isAuthenticated = require("../Authentication")

const Router=express.Router();

const {
    storeRole,
    getRole,
    getRoleDetails,
    updateRole,
    deleteRole
}=require("../controller/rolesController");

Router.post("/roles",isAuthenticated,storeRole)
Router.get("/roles",isAuthenticated,getRole)
Router.get("/roles/:id",isAuthenticated,getRoleDetails)
Router.put("/roles/:id",isAuthenticated,updateRole)
Router.delete("/roles/:id",isAuthenticated,deleteRole)

module.exports=Router;