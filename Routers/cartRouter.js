const express = require("express")
const isAuthenticated = require("../Authentication")

const Router = express.Router();

const {
    storeCart,
    getCart,
    getDetails,
    updateCart,
    deleteCart

} = require("../controller/cartController")

Router.post("/carts", isAuthenticated,storeCart)
Router.get("/carts",isAuthenticated, getCart)
Router.get("/carts/:id",isAuthenticated, getDetails)
Router.put("/carts/:id",isAuthenticated,updateCart)
Router.delete("/carts/:id",isAuthenticated,deleteCart)



module.exports = Router;