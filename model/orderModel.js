const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({
    user_id: String,
    Total_items: Number,
    product: String,
    Base_Price: Number,
    Sell_Price: Number,
    Billing_Address: String,
    Shipping_Address: String,
    Transaction_Status: String,
    Payment_mode: String,
    Payment_Status: String,
    Order_Status: String

});

const orderModel =mongoose.model("Orders",orderSchema);

module.exports = orderModel;