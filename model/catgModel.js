const mongoose=require("mongoose")

const categoriesSchema = mongoose.Schema({
    Name:String,
    slug:String,
    image:String,
    description:String
});

const categoriesModel=mongoose.model("Categories",categoriesSchema);

module.exports=categoriesModel;