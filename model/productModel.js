const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
    name: String,
    thumbnail: String,
    Product_Gallery: Array,
    description: String,
    Base_Price: Number,
    Sell_Price: Number,
    Category_name: String,
    Tags: String,
    Additional_information: String,
});

const productModel =mongoose.model("Products",productSchema);

module.exports = productModel;