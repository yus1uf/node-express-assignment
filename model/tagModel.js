const mongoose = require("mongoose");

const tagSchema = mongoose.Schema({
    Name:String,
    slug:String
});

const tagModel =mongoose.model("Tags",tagSchema);

module.exports = tagModel;