const mongoose = require("mongoose");

const cartSchema = mongoose.Schema({
    product: String,
    user: String,
    Product_qty: Number,
    Base_Price: Number,
    Sell_Price: Number,
    Total_Price: Number,
});

const cartModel =mongoose.model("Carts",cartSchema);

module.exports = cartModel;