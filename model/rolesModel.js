const mongoose=require("mongoose");

const roleSchema=mongoose.Schema({
    Name:String,
    slug:String
});

const roleModel=mongoose.model("Roles",roleSchema);

module.exports=roleModel;