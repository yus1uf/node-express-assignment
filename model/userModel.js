const mongoose=require("mongoose");

const userSchema=mongoose.Schema({
    first_name:String,
    last_name:String,
    email:String,
    role:String,
    profile_image:String,
    password:String
});

const userModel=mongoose.model("Users",userSchema);

module.exports = userModel;